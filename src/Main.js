import React from 'react';
import Child from './Child';
import { Button } from 'reactstrap';

class Mine extends React.Component{
    //state seperti variabel yang nilainya bisa berubah
    constructor(){
        super()
        this.state = {texts: [], color: '', counter: 0}
        this.ubahWarna = this.ubahWarna.bind(this)
        this.counterClick = this.counterClick.bind(this)
    }

    componentDidMount(){
        //untuk mengisi nilai awal variabel yang telah di inisialisasi lakukan di fungsi ini
        this.setState(
            {
                texts: ['Belajar ReactJs','Sungguh Menyenangkan', 'Asik Sekali'],
                color: 'green' 
            }
        );
    }

    ubahWarna(){
        if(this.state.color === 'green'){
            this.setState({color: 'blue'})
        }else{
            this.setState({color: 'green'})  
        }
    }

    counterClick(){
        this.setState(
            {
                counter: this.state.counter + 1
            }
        )
    }

    render(){
        // perulangan untuk menampilkan nilai dari list ke dalam Componen Child
        let tets = this.state.texts.map((text) => {
            return(
                <Child text={text} counterClick = {this.counterClick}/>
            )
        })

        let style = {color: this.state.color}

        return (
            <div style={style} onClick={this.ubahWarna}>
                <div id="mine">
                    Hello Dunia
                </div>
                <div>
                    tambahan baru 
                </div>
                {tets}
                {this.state.counter}
                <br/>
                <Button color="danger" onClick={this.ubahWarna}>Ubah Warna Text</Button>
            </div>
        );
    }
}

export default Mine;