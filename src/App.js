import React, { Component } from 'react';
import './App.css';
import Header from './Header';
import {Jumbotron, Button} from 'reactstrap';


class App extends Component {
  render() {
    return (
      <div>
        <Header />

        <Jumbotron>
          <h1 className="display-3">Selamat Datang</h1>
          <p className="lead">Ini adalah Contoh Hero Unit Jumbo tron</p>
          <hr className="my-2" />
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p className="lead">
            <Button color="primary">Learn More</Button>
          </p>
        </Jumbotron>
      </div>
    );
  }
}

export default App;
